//
//  CharactersViewController.m
//  Coya Coding Challenge
//
//  Created by Granit Gjevukaj on 13/03/2017.
//  Copyright © 2017 Granit Gjevukaj. All rights reserved.
//

#import "CharactersViewController.h"

// Views
#import "CharacterCell.h"

@interface CharactersViewController () <UITableViewDataSource, UITableViewDelegate>

// Navigation Bar
@property(strong, nonatomic) UIView* navBar;
@property(strong, nonatomic) UILabel* titleLabel;
@property(strong, nonatomic) UIButton* backButton;

// Characters TableView
@property(strong, nonatomic) UITableView* charactersTable;


@end

@implementation CharactersViewController

static NSString* DNDCharactersCellIdentifier = @"DNDCharactersCellIdentifier";


#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    // Setup Views
    [self setupNavigationBar];
    [self setupTableView];

}

#pragma mark - Setup Navigation Bar

-(void)setupNavigationBar {
    
    self.navBar = [[UIView alloc] init];
    self.navBar.backgroundColor = [UIColor colorWithRed:47.0/255.0 green:55.0/255.0 blue:62.0/255.0 alpha:1.0];
    [self.view addSubview:self.navBar];
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.text = @"Characters List";
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.textColor = [UIColor whiteColor];
    [self.navBar addSubview:self.titleLabel];
    
    self.backButton = [[UIButton alloc] init];
    [self.backButton setImage:[UIImage imageNamed:@"ic_back"] forState:UIControlStateNormal];
    [self.backButton addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.navBar addSubview:self.backButton];
    
}

#pragma mark - Setup TableView

- (void)setupTableView {
    
    self.charactersTable = [[UITableView alloc] init];
    self.charactersTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.charactersTable.delegate = self;
    self.charactersTable.dataSource = self;
    [self.charactersTable registerClass:[CharacterCell class] forCellReuseIdentifier:DNDCharactersCellIdentifier];
    [self.view addSubview:self.charactersTable];
}

#pragma mark - Layout

- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    // Navigation Bar
    {
        self.navBar.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 100.0);
        self.titleLabel.frame = CGRectMake(0, 50, CGRectGetWidth(self.view.bounds), 44.0);
        self.backButton.frame = CGRectMake(10.0, 50.0, 44, 44);
    }
    
    // TableView
    self.charactersTable.frame = CGRectMake(0, 100.0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds) - 100.0);
}

#pragma mark - TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.charactersArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CharacterCell *cell = [tableView dequeueReusableCellWithIdentifier:DNDCharactersCellIdentifier];
    
    if (cell == nil) {
        cell = [[CharacterCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:DNDCharactersCellIdentifier];
    }
    
    cell.character = self.charactersArray[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 150.0;
}

#pragma mark - Setter

-(void)setCharactersArray:(NSMutableArray *)charactersArray {
    _charactersArray = charactersArray;
}

#pragma mark - Button Action

-(void)backButtonAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
