//
//  CharacterCell.h
//  Coya Coding Challenge
//
//  Created by Granit Gjevukaj on 13/03/2017.
//  Copyright © 2017 Granit Gjevukaj. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Character.h"

@interface CharacterCell : UITableViewCell

// Model
@property(strong, nonatomic) Character* character;

@end
