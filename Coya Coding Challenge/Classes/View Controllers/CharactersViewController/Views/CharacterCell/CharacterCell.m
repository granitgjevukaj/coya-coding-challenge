//
//  CharacterCell.m
//  Coya Coding Challenge
//
//  Created by Granit Gjevukaj on 13/03/2017.
//  Copyright © 2017 Granit Gjevukaj. All rights reserved.
//

#import "CharacterCell.h"

@interface CharacterCell ()

@property(strong, nonatomic) UIView* containerView;

@property(strong, nonatomic) UILabel* classNameLabel;
@property(strong, nonatomic) UILabel* raceNameLabel;
@property(strong, nonatomic) UILabel* strengthLabel;
@property(strong, nonatomic) UILabel* dexterityLabel;
@property(strong, nonatomic) UILabel* constitutionLabel;
@property(strong, nonatomic) UILabel* intelligenceLabel;
@property(strong, nonatomic) UILabel* wisdomLabel;
@property(strong, nonatomic) UILabel* charismaLabel;

@property(strong, nonatomic) UIImageView* classImageView;

@end

@implementation CharacterCell

#pragma mark - Constructor

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initialise];
    }
    return self;
}

#pragma mark - Initialise

- (void)initialise {
    
    // Container View
    {
        self.containerView = [[UIView alloc] init];
        self.containerView.backgroundColor = [UIColor colorWithRed:47.0/255.0 green:55.0/255.0 blue:62.0/255.0 alpha:1.0];
        self.containerView.layer.cornerRadius = 6.0;
        self.containerView.layer.borderColor = [UIColor colorWithRed:47.0/255.0 green:55.0/255.0 blue:62.0/255.0 alpha:1.0].CGColor;
        self.containerView.layer.borderWidth = 0.4;
        self.containerView.clipsToBounds = YES;
        
        [self.contentView addSubview:self.containerView];
    }
    
    // Class Name
    {
        self.classNameLabel = [[UILabel alloc] init];
        self.classNameLabel.font = [UIFont boldSystemFontOfSize:16.0];
        self.classNameLabel.textColor = [UIColor whiteColor];
        self.classNameLabel.textAlignment = NSTextAlignmentLeft;
        
        [self.containerView addSubview:self.classNameLabel];
    }
    
    // Race Name
    {
        self.raceNameLabel = [[UILabel alloc] init];
        self.raceNameLabel.font = [UIFont boldSystemFontOfSize:14.0];
        self.raceNameLabel.textColor = [UIColor whiteColor];
        self.raceNameLabel.textAlignment = NSTextAlignmentLeft;
        
        [self.containerView addSubview:self.raceNameLabel];
    }
    
    // Strength Label
    {
        self.strengthLabel = [[UILabel alloc] init];
        self.strengthLabel.font = [UIFont systemFontOfSize:10.0];
        self.strengthLabel.textColor = [UIColor whiteColor];
        self.strengthLabel.textAlignment = NSTextAlignmentRight;
        
        [self.containerView addSubview:self.strengthLabel];
    }
    
    // Dexterity Label
    {
        self.dexterityLabel = [[UILabel alloc] init];
        self.dexterityLabel.font = [UIFont systemFontOfSize:10.0];
        self.dexterityLabel.textColor = [UIColor whiteColor];
        self.dexterityLabel.textAlignment = NSTextAlignmentRight;
        
        [self.containerView addSubview:self.dexterityLabel];
    }
    
    // Constitution Label
    {
        self.constitutionLabel = [[UILabel alloc] init];
        self.constitutionLabel.font = [UIFont systemFontOfSize:10.0];
        self.constitutionLabel.textColor = [UIColor whiteColor];
        self.constitutionLabel.textAlignment = NSTextAlignmentRight;
        
        [self.containerView addSubview:self.constitutionLabel];
    }
    
    // Intelligence Label
    {
        self.intelligenceLabel = [[UILabel alloc] init];
        self.intelligenceLabel.font = [UIFont systemFontOfSize:10.0];
        self.intelligenceLabel.textColor = [UIColor whiteColor];
        self.intelligenceLabel.textAlignment = NSTextAlignmentRight;
        
        [self.containerView addSubview:self.intelligenceLabel];
    }
    
    // Wisdom Label
    {
        self.wisdomLabel = [[UILabel alloc] init];
        self.wisdomLabel.font = [UIFont systemFontOfSize:10.0];
        self.wisdomLabel.textColor = [UIColor whiteColor];
        self.wisdomLabel.textAlignment = NSTextAlignmentRight;
        
        [self.containerView addSubview:self.wisdomLabel];
    }
    
    // Charisma Label
    {
        self.charismaLabel = [[UILabel alloc] init];
        self.charismaLabel.font = [UIFont systemFontOfSize:10.0];
        self.charismaLabel.textColor = [UIColor whiteColor];
        self.charismaLabel.textAlignment = NSTextAlignmentRight;
        
        [self.containerView addSubview:self.charismaLabel];
    }
    
    // Class Image
    {
        self.classImageView = [[UIImageView alloc] init];
        self.classImageView.contentMode = UIViewContentModeCenter;
        [self.containerView addSubview:self.classImageView];
    }
}

#pragma mark - Layout

-(void)layoutSubviews {
    
    [super layoutSubviews];
    
    // Container View
    {
        CGRect rect = CGRectZero;
        rect.origin.x = 20.0;
        rect.origin.y = 10.0;
        rect.size.width = CGRectGetWidth(self.contentView.bounds) - 40.0;
        rect.size.height = 140.0;
        
        self.containerView.frame = rect;
    }
    
    // Class Name
    {
        CGRect rect = CGRectZero;
        rect.origin.x = 20.0;
        rect.origin.y = 15.0;
        rect.size.width = 100.0;
        rect.size.height = 30.0;
        
        self.classNameLabel.frame = rect;
    }
    
    // Race Name
    {
        CGRect rect = CGRectZero;
        rect.origin.x = 20.0;
        rect.origin.y = self.classNameLabel.frame.origin.y + CGRectGetHeight(self.classNameLabel.bounds) + 5.0;
        rect.size.width = 100.0;
        rect.size.height = 30.0;
        
        self.raceNameLabel.frame = rect;
    }
    
    // Strength Label
    {
        CGRect rect = CGRectZero;
        rect.origin.x = CGRectGetWidth(self.containerView.bounds) - 20.0 - 150.0;
        rect.origin.y = 10.0;
        rect.size.width = 150.0;
        rect.size.height = 15.0;
        
        self.strengthLabel.frame = rect;
    }
    
    // Dexterity Label
    {
        CGRect rect = CGRectZero;
        rect.origin.x = CGRectGetWidth(self.containerView.bounds) - 20.0 - 150.0;
        rect.origin.y = self.strengthLabel.frame.origin.y + CGRectGetHeight(self.strengthLabel.bounds) + 5.0;
        rect.size.width = 150.0;
        rect.size.height = 15.0;
        
        self.dexterityLabel.frame = rect;
    }
    
    // Constitution Label
    {
        CGRect rect = CGRectZero;
        rect.origin.x = CGRectGetWidth(self.containerView.bounds) - 20.0 - 150.0;
        rect.origin.y = self.dexterityLabel.frame.origin.y + CGRectGetHeight(self.dexterityLabel.bounds) + 5.0;
        rect.size.width = 150.0;
        rect.size.height = 15.0;
        
        self.constitutionLabel.frame = rect;
    }
    
    // Intelligence Label
    {
        CGRect rect = CGRectZero;
        rect.origin.x = CGRectGetWidth(self.containerView.bounds) - 20.0 - 150.0;
        rect.origin.y = self.constitutionLabel.frame.origin.y + CGRectGetHeight(self.constitutionLabel.bounds) + 5.0;
        rect.size.width = 150.0;
        rect.size.height = 15.0;
        
        self.intelligenceLabel.frame = rect;
    }
    
    // Wisdom Label
    {
        CGRect rect = CGRectZero;
        rect.origin.x = CGRectGetWidth(self.containerView.bounds) - 20.0 - 150.0;
        rect.origin.y = self.intelligenceLabel.frame.origin.y + CGRectGetHeight(self.intelligenceLabel.bounds) + 5.0;
        rect.size.width = 150.0;
        rect.size.height = 15.0;
        
        self.wisdomLabel.frame = rect;
    }
    
    // Charisma Label
    {
        CGRect rect = CGRectZero;
        rect.origin.x = CGRectGetWidth(self.containerView.bounds) - 20.0 - 150.0;
        rect.origin.y = self.wisdomLabel.frame.origin.y + CGRectGetHeight(self.wisdomLabel.bounds) + 5.0;
        rect.size.width = 150.0;
        rect.size.height = 15.0;
        
        self.charismaLabel.frame = rect;
    }
    
    // Class Image
    {
        CGRect rect = CGRectZero;
        rect.origin.x = 20.0;
        rect.origin.y = self.raceNameLabel.frame.origin.y + CGRectGetHeight(self.raceNameLabel.bounds);
        rect.size.width = 50.0;
        rect.size.height = 50.0;
        
        self.classImageView.frame = rect;
    }
}

- (void)setCharacter:(Character *)character {
    _character = character;
    
    [self.classNameLabel setText:[self returnClassName:self.character.dndClass]];
    [self.raceNameLabel setText:[self returnRaceName:self.character.dndRace]];
    [self.classImageView setImage:[self returnClassImage:self.character.dndClass]];
    
    [self.strengthLabel setText:[NSString stringWithFormat:@"Strength: %i (Bonus: %i)", self.character.strength, self.character.bonusStrength]];
    [self.dexterityLabel setText:[NSString stringWithFormat:@"Dexterity: %i (Bonus: %i)", self.character.dexterity, self.character.bonusDexterity]];
    [self.constitutionLabel setText:[NSString stringWithFormat:@"Constitution: %i (Bonus: %i)", self.character.constitution, self.character.bonusConstitution]];
    [self.intelligenceLabel setText:[NSString stringWithFormat:@"Intelligence: %i (Bonus: %i)", self.character.intelligence, self.character.bonusIntelligence]];
    [self.wisdomLabel setText:[NSString stringWithFormat:@"Wisdom: %i (Bonus: %i)", self.character.wisdom, self.character.bonusWisdom]];
    [self.charismaLabel setText:[NSString stringWithFormat:@"Charisma: %i (Bonus: %i)", self.character.charisma, self.character.bonusCharisma]];

}

-(UIImage *)returnClassImage:(DNDClass)dndClass {
    
    UIImage* image;
    
    switch (dndClass) {
            
        case DNDClassWarrior:
            image = [UIImage imageNamed:@"ic_warrior"];
            break;
            
        case DNDClassWizard:
            image = [UIImage imageNamed:@"ic_wizard"];
            break;
            
        case DNDClassRanger:
            image = [UIImage imageNamed:@"ic_ranger"];
            break;
            
        default:
            break;
    }
    return image;
}

-(NSString *)returnClassName:(DNDClass)dndClass {
    
    NSString* name;
    
    switch (dndClass) {
            
        case DNDClassWarrior:
            name = @"Warrior";
            break;
            
        case DNDClassWizard:
            name = @"Wizzard";
            break;
            
        case DNDClassRanger:
            name = @"Ranger";
            break;
            
        default:
            break;
    }
    return name;
}

-(NSString *)returnRaceName:(DNDRace)dndRace {
    
    NSString* name;
    
    switch (dndRace) {
            
        case DNDRaceDwarf:
            name = @"Dwarf";
            break;
            
        case DNDRaceHuman:
            name = @"Human";
            break;
            
        case DNDRaceElf:
            name = @"Elf";
            break;
            
        default:
            break;
    }
    return name;
}


@end
