//
//  CharactersViewController.h
//  Coya Coding Challenge
//
//  Created by Granit Gjevukaj on 13/03/2017.
//  Copyright © 2017 Granit Gjevukaj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CharactersViewController : UIViewController

@property(strong, nonatomic) NSMutableArray* charactersArray;

@end
