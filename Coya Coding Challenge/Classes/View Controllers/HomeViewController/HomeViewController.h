//
//  HomeViewController.h
//  Coya Coding Challenge
//
//  Created by Granit Gjevukaj on 13/03/2017.
//  Copyright © 2017 Granit Gjevukaj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController

// Actions
- (IBAction)generateCharactersAction:(id)sender;

@end
