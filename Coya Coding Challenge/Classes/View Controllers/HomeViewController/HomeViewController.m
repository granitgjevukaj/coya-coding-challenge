//
//  HomeViewController.m
//  Coya Coding Challenge
//
//  Created by Granit Gjevukaj on 13/03/2017.
//  Copyright © 2017 Granit Gjevukaj. All rights reserved.
//

#import "HomeViewController.h"

// ViewControllers
#import "CharactersViewController.h"

// Engine
#import "CharacterEngine.h"

#import "MBProgressHUD.h"

@interface HomeViewController ()

@property(strong, nonatomic) NSMutableArray* charactersArray;

@end

@implementation HomeViewController

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
}


#pragma mark - Generate Characters

- (IBAction)generateCharactersAction:(id)sender {
    
    [[CharacterEngine sharedInstance] generateCharacters:^(BOOL success, NSArray *characters) {
        if (success) {
            self.charactersArray = [NSMutableArray arrayWithArray:characters];
            
            CharactersViewController* charactersVC = [CharactersViewController new];
            charactersVC.charactersArray = self.charactersArray;
            [self.navigationController pushViewController:charactersVC animated:YES];
        }
    }];
}
@end
