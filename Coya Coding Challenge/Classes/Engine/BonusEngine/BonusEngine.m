//
//  BonusEngine.m
//  Coya Coding Challenge
//
//  Created by Granit Gjevukaj on 14/03/2017.
//  Copyright © 2017 Granit Gjevukaj. All rights reserved.
//

#import "BonusEngine.h"

@implementation BonusEngine

#pragma mark - Calculate Bonus

- (int)calculateBonusForCharacter:(NSInteger)characteristic type:(DNDCharacteristic)type race:(DNDRace)race andClass:(DNDClass)dndClass {
    
    int bonus;
    
    // Base Bonus
    if (characteristic == 4 || characteristic == 5) {
        bonus = -3;
    } else if (characteristic == 6 || characteristic == 7) {
        bonus = -2;
    } else if (characteristic == 8 || characteristic == 9) {
        bonus = -1;
    } else if (characteristic == 10 || characteristic == 11) {
        bonus = 0;
    } else if (characteristic == 12 || characteristic == 13) {
        bonus = 1;
    } else if (characteristic == 14 || characteristic == 15) {
        bonus = 2;
    } else if (characteristic == 16 || characteristic == 17) {
        bonus = 3;
    } else if (characteristic == 18 || characteristic == 19) {
        bonus = 4;
    } else if (characteristic == 20 || characteristic == 21) {
        bonus = 5;
    } else if (characteristic == 22 || characteristic == 23) {
        bonus = 6;
    } else if (characteristic == 24) {
        bonus = 7;
    }
    
    // Race Bonus
    if (race == DNDRaceDwarf) {
        
        if (type == DNDCharacteristicStrength) {
            bonus += 2;
        }
        
        if (type == DNDCharacteristicConstitution) {
            bonus += 2;
        }
        
        if (type == DNDCharacteristicDexterity) {
            bonus += -2;
        }
        
        if (type == DNDCharacteristicCharisma) {
            bonus += -2;
        }
    }
    
    if (race == DNDRaceElf) {
        
        if (type == DNDCharacteristicDexterity) {
            bonus += 2;
        }
        
        if (type == DNDCharacteristicStrength) {
            bonus += -2;
        }
    }
    
    
    // Class Bonus
    if (dndClass == DNDClassWarrior) {
        if (race == DNDRaceDwarf) {
            if (type == DNDCharacteristicStrength) {
                bonus += 3;
            }
        }
        
        if (race == DNDRaceHuman) {
            if (type == DNDCharacteristicStrength) {
                bonus += 2;
            }
        }
    }
    
    if (dndClass == DNDClassWizard) {
        if (race == DNDRaceElf || race == DNDRaceHuman) {
            if (type == DNDCharacteristicIntelligence) {
                bonus += 1;
            }
        }
    }
    
    if (dndClass == DNDClassRanger) {
        if (race == DNDRaceElf) {
            if (type == DNDCharacteristicDexterity) {
                bonus += 2;
            }
        }
    }
    
    return bonus;
}

#pragma mark - Singleton

+ (id)sharedInstance {
    
    static dispatch_once_t pred = 0;
    __strong static id _sharedInstance = nil;
    dispatch_once(&pred, ^{
        _sharedInstance = [[self alloc] init];
        BonusEngine* engine = (BonusEngine *)_sharedInstance;
    });
    return _sharedInstance;
    
}

- (id)init {
    self = [super init];
    if(self) {
    }
    return self;
}


@end
