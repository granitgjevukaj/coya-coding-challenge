//
//  BonusEngine.h
//  Coya Coding Challenge
//
//  Created by Granit Gjevukaj on 14/03/2017.
//  Copyright © 2017 Granit Gjevukaj. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Character.h"

@interface BonusEngine : NSObject

-(int)calculateBonusForCharacter:(NSInteger)characteristic type:(DNDCharacteristic)type race:(DNDRace)race andClass:(DNDClass)dndClass;

+(id)sharedInstance;

@end
