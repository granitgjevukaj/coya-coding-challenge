//
//  CharacterEngine.m
//  Coya Coding Challenge
//
//  Created by Granit Gjevukaj on 13/03/2017.
//  Copyright © 2017 Granit Gjevukaj. All rights reserved.
//

#import "CharacterEngine.h"

#import "Character.h"

@implementation CharacterEngine

#pragma mark - Character Generation

-(void)generateCharacters:(CharactersCompletion)completion {
    
    NSMutableArray * tempArray = [[NSMutableArray alloc]init];
    
    for (int i = 0; i < 100; i++) {
        Character* character = [Character initCharacterWithRace:[self generateRandomRace] andClass:[self generateRandomClass]];
        [tempArray addObject:character];
    }
    
    completion(YES, tempArray);
}

- (DNDRace)generateRandomRace {
    return arc4random_uniform(3);
}

- (DNDClass)generateRandomClass {
    return arc4random_uniform(3);
}

#pragma mark - Singleton

+ (id)sharedInstance {
    
    static dispatch_once_t pred = 0;
    __strong static id _sharedInstance = nil;
    dispatch_once(&pred, ^{
        _sharedInstance = [[self alloc] init];
        CharacterEngine* engine = (CharacterEngine *)_sharedInstance;
        engine.characters = [NSMutableArray array];
    });
    return _sharedInstance;
    
}

- (id)init {
    self = [super init];
    if(self) {
    }
    return self;
}

@end
