//
//  CharacterEngine.h
//  Coya Coding Challenge
//
//  Created by Granit Gjevukaj on 13/03/2017.
//  Copyright © 2017 Granit Gjevukaj. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Character.h"

typedef void (^CharactersCompletion)(BOOL success, NSArray* characters);

@interface CharacterEngine : NSObject

@property (nonatomic, retain) NSMutableArray* characters;

-(void)generateCharacters:(CharactersCompletion)completion;

+(id)sharedInstance;

@end
