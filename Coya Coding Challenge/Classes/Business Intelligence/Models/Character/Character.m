//
//  Character.m
//  Coya Coding Challenge
//
//  Created by Granit Gjevukaj on 13/03/2017.
//  Copyright © 2017 Granit Gjevukaj. All rights reserved.
//

#import "Character.h"

#import "BonusEngine.h"

@implementation Character

+ (Character *)initCharacterWithRace:(DNDRace)race andClass:(DNDClass)dndClass {
    
    Character* character = [Character new];
    
    character.dndRace = race;
    character.dndClass = dndClass;
    
    character.strength = arc4random_uniform(20) + 4;
    character.dexterity = arc4random_uniform(20) + 4;
    character.constitution = arc4random_uniform(20) + 4;
    character.intelligence = arc4random_uniform(20) + 4;
    character.wisdom = arc4random_uniform(20) + 4;
    character.charisma = arc4random_uniform(20) + 4;
    
    character.bonusStrength = [[BonusEngine sharedInstance] calculateBonusForCharacter:character.strength type:DNDCharacteristicStrength race:race andClass:dndClass];
    character.bonusDexterity = [[BonusEngine sharedInstance] calculateBonusForCharacter:character.dexterity type:DNDCharacteristicDexterity race:race andClass:dndClass];
    character.bonusConstitution = [[BonusEngine sharedInstance] calculateBonusForCharacter:character.constitution type:DNDCharacteristicConstitution race:race andClass:dndClass];
    character.bonusIntelligence = [[BonusEngine sharedInstance] calculateBonusForCharacter:character.intelligence type:DNDCharacteristicIntelligence race:race andClass:dndClass];
    character.bonusWisdom = [[BonusEngine sharedInstance] calculateBonusForCharacter:character.wisdom type:DNDCharacteristicWisdom race:race andClass:dndClass];
    character.bonusCharisma = [[BonusEngine sharedInstance] calculateBonusForCharacter:character.charisma type:DNDCharacteristicCharisma race:race andClass:dndClass];

    return character;
}

@end
