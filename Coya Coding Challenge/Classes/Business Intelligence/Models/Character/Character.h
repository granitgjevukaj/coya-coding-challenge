//
//  Character.h
//  Coya Coding Challenge
//
//  Created by Granit Gjevukaj on 13/03/2017.
//  Copyright © 2017 Granit Gjevukaj. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Character : NSObject

// Enums
typedef NS_ENUM(NSUInteger, DNDRace) {
    DNDRaceDwarf = 0,
    DNDRaceElf = 1,
    DNDRaceHuman = 2
};

typedef NS_ENUM(NSUInteger, DNDClass) {
    DNDClassWarrior = 0,
    DNDClassWizard = 1,
    DNDClassRanger = 2
};

typedef NS_ENUM(NSUInteger, DNDCharacteristic) {
    DNDCharacteristicStrength = 0,
    DNDCharacteristicDexterity = 1,
    DNDCharacteristicConstitution = 2,
    DNDCharacteristicIntelligence = 3,
    DNDCharacteristicWisdom = 4,
    DNDCharacteristicCharisma = 5
};

// Class & Race
@property (nonatomic, assign) DNDRace dndRace;
@property (nonatomic, assign) DNDClass dndClass;

// Characteristics
@property(assign, nonatomic) int strength;
@property(assign, nonatomic) int dexterity;
@property(assign, nonatomic) int constitution;
@property(assign, nonatomic) int intelligence;
@property(assign, nonatomic) int wisdom;
@property(assign, nonatomic) int charisma;

// Bonus
@property(assign, nonatomic) int bonusStrength;
@property(assign, nonatomic) int bonusDexterity;
@property(assign, nonatomic) int bonusConstitution;
@property(assign, nonatomic) int bonusIntelligence;
@property(assign, nonatomic) int bonusWisdom;
@property(assign, nonatomic) int bonusCharisma;

// Private Method
+(Character *)initCharacterWithRace:(DNDRace)race andClass:(DNDClass)dndClass;

@end
